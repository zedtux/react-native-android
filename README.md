# React-Native Docker image for Android with Fastlane

[React-native](https://facebook.github.io/react-native/) is a Facebook framework
which make fast mobile application using native components while you're writing
React code.

[Fastlane](https://fastlane.tools) is an automation tool made for mobile app
development.

This Docker image is based on Ubuntu 16.04.3 LTS including :

 - Java 1.8.0_171
 - Ruby 2.4.4p296 (Not using Ruby 2.5 because of [this issue](https://github.com/fastlane/fastlane/issues/11629).)
 - Node v8.11.2
 - Yarn 1.7.0

It also includes the **Android SDK version 26** and the **Android emulator**.

#### iOS in Docker

iOS mobile apps can't be tested using Docker as it requires the Xcode world and
it can't be packaged within a Docker container.
